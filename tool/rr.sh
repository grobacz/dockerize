#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BINARY=$([ -f "$DIR/../tool/rr" ] && echo "$DIR/../tool/rr" || echo $(which rr))

if [ ! -f $BINARY ]
then
	echo "rr binary could not be found";
	exit 1;
fi

cp $BINARY "$1";
exit 0;
