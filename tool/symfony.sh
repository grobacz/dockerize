#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BINARY=$([ -f "$DIR/../tool/symfony" ] && echo "$DIR/../tool/symfony" || echo $(which symfony))

if [ ! -f $BINARY ]
then
	echo "symfony binary could not be found";
	exit 1;
fi

cp $BINARY "$1";
exit 0;
