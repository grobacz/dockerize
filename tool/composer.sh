#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BINARY=$([ -f "$DIR/../tool/composer" ] && echo "$DIR/../tool/composer" || echo $(which composer))

if [ ! -f $BINARY ]
then
	echo "composer binary could not be found";
	exit 1;
fi

cp $BINARY "$1";
exit 0;
