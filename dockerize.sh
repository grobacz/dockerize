#!/bin/bash

FRAMEWORK=$(dockerize/framework_detector.sh $1 | tr '[:upper:]' '[:lower:]');

if [ -z "$FRAMEWORK" ]
then
	echo "Framework not supported"
	exit 0;
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cp "$DIR/dockerfile/$FRAMEWORK" $1/Dockerfile

case $FRAMEWORK in
	'symfony')
		$DIR/tool/symfony.sh $1
		$DIR/tool/composer.sh $1
		;;
	'roadrunner')
		$DIR/tool/roadrunner.sh $1
		$DIR/tool/composer.sh $1
		;;
	'yii2')
		;&
	'apiplatform')
		$DIR/tool/composer.sh $1
		;;
esac
cd $1;

IMAGE_ID=$(docker build -q . | cut -d ':' -f2);
CONTAINER_ID=$(docker create -p 8000:8000 $IMAGE_ID);
if [ ! -z "$CONTAINER_ID" ]
then
	docker start $CONTAINER_ID && echo "Container created. Check localhost:8000";
else
	echo "Failed to start container";
fi

