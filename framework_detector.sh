#!/bin/bash

DIR="$( cd "$( dirname "$i" )" && pwd )";

for var in "$@"
do
	test ! -d "$DIR/$var" && continue;
	cd "$DIR/$var";

	composer show api-platform/api-pack > /dev/null 2>&1 && echo "APIPLATFORM" && continue;
	composer show symfony/framework-bundle > /dev/null 2>&1 && echo "SYMFONY" && continue;
	composer show yiisoft/yii2 > /dev/null 2>&1 && echo "YII2" && continue;
	composer show spiral/roadrunner > /dev/null 2>&1 && echo "ROADRUNNER" && continue;
	test -f 'manage.py' && echo "DJANGO" && continue;

done
exit 0;
